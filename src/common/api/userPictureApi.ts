import { Request } from '@/common/api/request';

export class UserPictureApi extends Request {
    public async getUserPictures(userId: string, page: number, perPage: number) {
        this.endpoint = `/users/${userId}/pictures`;
        this.params = {
            page,
            perPage,
        };
        const { data } = await this.get();
        return  data;
    }
}

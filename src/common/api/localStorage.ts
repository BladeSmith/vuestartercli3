export class LocalStorage {
    public static async getUserStoredInfo() {
        let tokenObject = null;
        try {
            tokenObject = localStorage.getItem(this.tokenName);
        } catch (error) {
            return false;
        }
        if (tokenObject === null) {
            return false;
        }
        tokenObject = JSON.parse(tokenObject);
        return tokenObject;
    }

    public static async updateUserStoredInfo(tokenObject: object) {
        try {
            localStorage.setItem(this.tokenName, JSON.stringify(tokenObject));
            return true;
        } catch (error) {
            return false;
        }
    }

    public static async deleteStoredInfo() {
        try {
            localStorage.removeItem(this.tokenName);
            return true;
        } catch (error) {
            return false;
        }
    }
    private static tokenName: 'USER_TOKEN_ITEM';
}

import axios from 'axios';
import { LocalStorage} from '@/common/api/localStorage';

/**
 * Request class used to generate api requests
 * The class use the Builder design pattern
 */
export abstract class Request {

    /**
     * Concatenate host and version specified in the configuration
     * Use o be a base for any request
     * @returns {string}
     */
    private static baseUrl() {
        const host = 'http://api.ugram.net/';
        const version = '';

        return `${host}/${version}`;
    }

    private static async getToken() {
        const accessToken = await LocalStorage.getUserStoredInfo();
        if (accessToken === false) {
            throw new Error('Invalid Access Token');
        }
        return accessToken;
    }
    protected endpoint: string;
    protected headers: object;
    protected params: object;
    protected data: object;
    protected responseType: string;
    protected method: string;
    protected authed: boolean;

    protected constructor() {
        this.endpoint = '';
        this.headers = {};
        this.params = {};
        this.data = {};
        this.responseType = 'json';
        this.method = 'GET';
        this.authed = true;
    }

    /**
     * Make a GET request
     * @returns {Promise<*>}
     */
    protected async get() {
        this.method = 'GET';
        this.data = {};
        return this.request();
    }

    /**
     * Make a POST request
     * @returns {Promise<*>}
     */
    protected async post() {
        this.method = 'POST';
        return this.request();
    }

    /**
     * Make a PATCH request
     * @returns {Promise<*>}
     */
    protected async patch() {
        this.method = 'PATCH';
        return this.request();
    }

    /**
     * Make a DELETE pattern
     * @returns {Promise<*>}
     */
    protected async delete() {
        this.method = 'DELETE';
        return this.request();
    }

    /**
     * Make a LOCK pattern
     * @returns {Promise<*>}
     */
    protected async lock() {
        this.method = 'LOCK';
        return this.request();
    }

    /**
     * Function used to do the real request
     * @returns {Object}
     */
    private async request() {
        let headers = {};

        if (this.authed) {
            const accessToken = await Request.getToken();
            const bearer = `Bearer ${accessToken}`;

            const defaultAuthorizationHeader = { Authorization: bearer };
            headers = Object.assign(defaultAuthorizationHeader, this.headers);
        }

        const base = Request.baseUrl();

        const options = {
            url: this.endpoint,
            method: this.method,
            baseURL: base,
            headers,
            params: this.params,
            data: this.data,
            responseType: this.responseType,
        };

        return axios(options);
    }
}

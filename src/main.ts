import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import Vuetify from 'vuetify';
import router from './router/router';
import store from './store/store';
import './registerServiceWorker';

Vue.config.productionTip = false;
Vue.use(Vuetify);
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
